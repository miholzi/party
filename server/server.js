var express = require('express')
,   app = express()
,   server = require('http').createServer(app)
,   io = require('socket.io').listen(server)
,   osc = require('node-osc')
,   conf = require('./config.json');

// Webserver
// auf den Port x schalten
server.listen(conf.port);
app.configure(function(){
	// statische Dateien ausliefern
	app.use(express.static(__dirname + '/public'));
});

// route public index file
app.get('/', function (req, res) {
	res.sendFile(__dirname + '/public/index.html');
});


// ---------- messages ------ //

var currentIp = '10.0.0.14';
var readyToMakeCocktail = true;

var maschine_client = new osc.Client(currentIp, 8000);
var processing_client = new osc.Client(currentIp, 8001);
var msg_from_robot = new osc.Server(8003, currentIp);




// lisen to robot
msg_from_robot.on("msgFromRobot", function (msg, rinfo)
{
	if (msg[1]=="cocktailFinish") {
		readyToMakeCocktail = true;
		console.log("cocktail finish place");
		io.sockets.emit('msgFromServerToBrowser', { 'msgToBrowser':'openToOrder', 'nameWhoGetsCocktail' : 'roboniko'});

	};
});






// listen to browser clients
	io.sockets.on('connection', function (socket) {
		socket.on('msgFromBrowser', function (data) {
			if (readyToMakeCocktail==true) {
				io.sockets.emit('msgFromServerToBrowser', { 'msgToBrowser':'orderClosed', 'nameWhoGetsCocktail' : data.name});
				readyToMakeCocktail = false;
				maschine_client.send('msgToRobot',data.cocktailnummer);
				processing_client.send('msgToProcessingCocktailnummer',parseInt(data.cocktailnummer));
				processing_client.send('msgToProcessingUsername',data.name);
		}
		else {
			console.log("coctail in work");
		}
	});
});




